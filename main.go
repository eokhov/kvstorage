package main

import (
	"fmt"
	"net/http"
	"sync"
)

type KVStorage interface {
	Get(key string) (interface{}, error)
	Put(key string, val interface{}) error
	Delete(key string) error
}

type Storage struct {
	mu    sync.RWMutex
	store map[string]string
}

func (s *Storage) Get(key string) string {
	s.mu.RLock()
	defer s.mu.RUnlock()
	return s.store[key]
}

func (s *Storage) Put(key string, val string) {
	s.mu.Lock()
	s.store[key] = val
	s.mu.Unlock()
}

func (s *Storage) Delete(key string) {
	s.mu.Lock()
	delete(s.store, key)
	s.mu.Unlock()
}

func main() {
	store := Storage{store: make(map[string]string)}

	http.HandleFunc("/put", func(rw http.ResponseWriter, r *http.Request) {
		rKey := r.URL.Query().Get("key")
		rVal := r.URL.Query().Get("value")

		go store.Put(rKey, rVal)

		fmt.Fprintln(rw, "Data added")
	})

	http.HandleFunc("/get", func(rw http.ResponseWriter, r *http.Request) {
		rKey := r.URL.Query().Get("key")

		val := store.Get(rKey)

		fmt.Fprintln(rw, val)
	})

	http.HandleFunc("/delete", func(rw http.ResponseWriter, r *http.Request) {
		rKey := r.URL.Query().Get("key")

		go store.Delete(rKey)

		fmt.Fprintln(rw, "Data deleted")
	})

	http.ListenAndServe(":8080", nil)
}
